package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
    def pascal(c: Int, r: Int): Int = {
      if (c == 0 || c == r) 1
      else pascal(c-1, r-1) + pascal(c, r-1)
    }
  
  /**
   * Exercise 2
   */
    def balance(chars: List[Char]): Boolean = {
      def balance_parentheses(chars: List[Char], i: Int): Int = {
        if (chars.isEmpty || i < 0) i
        else if (chars.head == '(')
          balance_parentheses(chars.tail, i + 1)
        else if (chars.head == ')')
          balance_parentheses(chars.tail, i - 1)
        else
          balance_parentheses(chars.tail, i)
      }
      balance_parentheses(chars, 0) == 0
    }
  
  /**
   * Exercise 3
   */
    def countChange(money: Int, coins: List[Int]): Int = {
      def loop(money: Int, coins: List[Int], acc: Int): Int = {
        if (money == 0) 1
        else if (money < 0 || coins.isEmpty) 0
        else 
          loop(money - coins.head, coins, acc) +
          loop(money, coins.tail, acc + 1)
      }
      loop(money, coins, 0)
    }
  }
