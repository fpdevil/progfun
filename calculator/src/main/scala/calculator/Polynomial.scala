package calculator

object Polynomial {
  def computeDelta(a: Signal[Double], b: Signal[Double],
      c: Signal[Double]): Signal[Double] = {
    Signal {
      b() * b() - 4 * a() * c()
    }
  }

  def computeSolutions(a: Signal[Double], b: Signal[Double],
      c: Signal[Double], delta: Signal[Double]): Signal[Set[Double]] = {
    Signal {
      import scala.math.sqrt
      delta() match {
        case del if del > 0 =>
          Set((((-1 * b()) + sqrt(delta()))) / (2 * a()), 
              (((-1 * b()) - sqrt(delta()))) / (2 * a()))
        case del if del == 0 =>
          Set((-1 * b()) / (2 * a()))
        case _ =>
          Set()
      }
    }
  }
}
