package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  lazy val genHeap: Gen[H] = for {
    z <- arbitrary[Int]
    h <- oneOf(const(empty), genHeap)
  } yield insert(z, h)
  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)
  

  property("gen1") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }
  
  // my implementations
  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }
  
  property("min2") = forAll { (a: Int, b: Int) =>
    val h = insert(b, insert(a, empty))
    val m = if (a <= b) a else b
    findMin(h) == m
  }
  
  property("empty_heap") = forAll { a: Int =>
    val h = insert(a, empty)
    isEmpty(deleteMin(h))
  }
  
  property("delmin") = forAll { a: Int =>
    val h = insert(a, empty)
    deleteMin(h) == empty
  }
  
  property("delmeld") = forAll { (a: H, b: H) =>
    val mina = findMin(a)
    val minb = findMin(b)
    val minmeld = findMin(meld(a, b))
    (mina == minmeld) || (minb == minmeld)
  }
  
  property("sorted_seq_elems") = forAll { a: H =>
    def isLessThan(x: A, xs: H): Boolean =
      if (isEmpty(xs))
        true
      else {
        val m = findMin(xs)
        val d = deleteMin(xs)
        if (x <= m)
          isLessThan(m, d)
        else
          false
      }
    val y = findMin(a)
    val ys = deleteMin(a)
    isLessThan(y, ys) == true
  }
  
  property("min_of_meld") = forAll { (h1: H, h2: H) =>
    def areHeapsEqual(h1: H, h2: H): Boolean =
      if (isEmpty(h1) && isEmpty(h2))
        true
      else {
        val m1 = findMin(h1)
        val m2 = findMin(h2)
        m1 == m2 &&
        areHeapsEqual(deleteMin(h1), deleteMin(h2))
      }
    areHeapsEqual(meld(h1, h2), meld(deleteMin(h1), deleteMin(h2)))
  }
  
  // generator for Maps
/*  lazy val genMap: Gen[Map[Int, Int]]) = for {
    k <- arbitrary[Int]
    v <- arbitraty[Int]
    m <- oneOf(const(Map.empty[Int, Int]), genMap)
  } yield m.updated(k, v)*/

}
